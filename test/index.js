const merge = require('../index')

const obj1 = {
    config: {
        title: 'config',
        test: true,
        plugins: [
            'test1',
            'test2'
        ]
    }
}

const obj2 = {
    new: 'entry',
    config: {
        test: false,
        plugins: [
            'test3'
        ]
    }
}

console.log(obj1)
console.log(obj2)
console.log('===========================================================')
console.log(merge(obj1, obj2))