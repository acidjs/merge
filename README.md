# Merge ♻️

### Recursively merge all objects in series.

[![pipeline status](https://gitlab.com/acidjs/merge/badges/master/pipeline.svg)](https://gitlab.com/acidjs/merge/-/commits/master)
[![npm (scoped)](https://img.shields.io/npm/v/@acidjs/merge)](https://www.npmjs.com/package/@acidjs/merge)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/acidjs/merge/-/blob/master/LICENSE.md)

## Install
```
npm install @acidjs/merge
```

## Usage
```js
const merge = require('@acidjs/merge')

const obj1 = {
    config: {
        title: 'config',
        test: true,
        plugins: [
            'test1',
            'test2'
        ]
    }
}

const obj2 = {
    new: 'entry',
    config: {
        test: false,
        plugins: [
            'test3'
        ]
    }
}

console.log(merge(obj1, obj2))

// output
{ 
    new: 'entry',
    config: { 
        title: 'config', 
        test: false, 
        plugins: [ 'test3', 'test2' ] 
    }
}
```

## Configure
coming soon...

## License
MIT License

Copyright (c) 2020 Michael Arakilian

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.