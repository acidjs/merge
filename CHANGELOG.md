# Changelog

## v0.1.0
- A direct copy of [lodash.merge](https://www.npmjs.com/package/lodash.merge) v4.6.2
- Later releases will include configuration options.